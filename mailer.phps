<?php

// SMTP needs accurate times, and the PHP time zone MUST be set
	header('Content-Type: text/html; charset=utf-8');

	// Host Details
	include_once("host.php");
	date_default_timezone_set('Asia/Kolkata');

	require 'PHPMailerAutoload.php';

	//Create a new PHPMailer instance
	$mail = new PHPMailer;

	//Tell PHPMailer to use SMTP
	$mail->isSMTP();

	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages

	$mail->SMTPDebug = 2;

	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';

	//Set the hostname of the mail server
	$mail->Host = 'smtp.gmail.com';

	// $mail->Host = gethostbyname('smtp.gmail.com');
	// if your network does not support SMTP over IPv6

	//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
	$mail->Port = 587;

	//Set the encryption system to use - ssl (deprecated) or tls
	$mail->SMTPSecure = 'tls';

	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;

	//Username to use for SMTP authentication - use full email address for gmail
	// Please enable it to yes https://www.google.com/settings/security/lesssecureapps

	$mail->Username = "sample@domail.com";

	//Password to use for SMTP authentication
	$mail->Password = "password";

	//Set who the message is to be sent from
	$mail->setFrom('sample@domail.com', 'Reports');

	//Set an alternative reply-to address
	$mail->addReplyTo('shirshandu@yahoo.com', 'shirshandu');

	//Set who the message is to be sent to

	//query to fetch emailid and name
	$getQuery = "select `id`,`email`,`name` from emails";
	$getExec = mysqli_query($db, $getQuery);
	$result = array();
	while ($row = mysqli_fetch_assoc($getExec)) {
		$data = array();
			$data['id'] = $row['id'];
			$data['email'] = $row['email'];
			$data['name'] = $row['name'];                                               
		array_push($result, $data);
	}

	// Fetch all email id from db
	foreach ($result  as $variable  ) {
		$emails = $variable['email'];
		$mail->addAddress($emails);
	}
?>